from flask import Blueprint, render_template, request
from flask_login import login_required, current_user
from . import db, user_list
from .models import User, Message, Chat
from datetime import datetime

main = Blueprint('main', __name__)
users = {}
messages_sent = {}

@main.route('/')
@login_required
def index():
    return render_template('index.html', name=current_user.name)

@main.route('/index/users')
@login_required
def get_users():
    return user_list

@main.route('/index/message', methods=['POST'])
@login_required
def send_message():
    # Route used for sending messages between 2 users

    data = request.form
    print('recieved new msg: ', data)
    message = data.get('msg')
    
    # the ids in the chat table are ordered by size
    userA = int(current_user.get_id())
    userB = int(data.get('destination'))
    if userA > userB:
        userA, userB = userB, userA
        
    chat = Chat.query.filter_by(A_userId=userA, B_userId=userB).first()
    if not chat:  # creating a new chat for the 2 users if it does not exist
        chat = Chat(A_userId=userA, B_userId=userB)
        db.session.add(chat)
        db.session.commit()

    # adding a new message to the chat
    chat_id = chat.id
    new_msg = Message(chatId=chat_id,sender=current_user.id,msg=message)
    db.session.add(new_msg)
    db.session.commit()

    # updating the last message the user got
    current_id = int(current_user.get_id())
    dest = int(data.get('destination'))
    messages_sent[current_id][dest] = str(new_msg.date)
    
    return {'status': 'success'}



@main.route('/index/get_messages', methods=['POST'])
@login_required
def get_messages():
    # Route used for getting the chat history with another user

    data = request.form
    userA = int(current_user.get_id())
    userB = int(data.get('destination'))
    if userA > userB:
        userA, userB = userB, userA
        
    chat = Chat.query.filter_by(A_userId=userA, B_userId=userB).first()
    if not chat:  # creating a new chat for the 2 users if it does not exist
        chat = Chat(A_userId=userA, B_userId=userB)
        db.session.add(chat)
        db.session.commit()

    # getting all messages from the chat
    chat_id = chat.id
    msg_dict = {'messages': []}
    messages = Message.query.filter_by(chatId=chat_id).all()
    for message in messages:
        msg_dict['messages'] += [{'sender': (User.query.filter_by(id = message.sender).first()).name, 'msg': message.msg, 'date': str(message.date)}]

    # updating the last message the user got
    current_id = int(current_user.get_id())
    dest = int(data.get('destination'))
    if not (current_id in messages_sent):
        messages_sent[current_id] = {}
        messages_sent[current_id][dest] = msg_dict['messages'][-1]['date']
    elif not (dest in messages_sent[current_id]):
        messages_sent[current_id][dest] = msg_dict['messages'][-1]['date']

    print(msg_dict)
    return msg_dict


@main.route('/index/update_messages', methods=['POST'])
@login_required
def update_messages():
    # Route used for sending new messages received to the user

    data = request.form  # get data from form on website  
    # the ids in the chat table are ordered by size
    userA = int(current_user.get_id())
    userB = int(data.get('destination'))
    if userA > userB:
        userA, userB = userB, userA
        
    chat = Chat.query.filter_by(A_userId=userA, B_userId=userB).first()

    if not chat: # creating a new chat for the 2 users if it does not exist
        chat = Chat(A_userId=userA, B_userId=userB)
        db.session.add(chat)  # create a new one
        db.session.commit()

    chat_id = chat.id
    msg_dict = {'messages': []}
    messages = Message.query.filter_by(chatId=chat_id).all()  # get all messages
    for message in messages:
        msg_dict['messages'] += [{'sender': (User.query.filter_by(id = message.sender).first()).name, 'msg': message.msg, 'date': str(message.date)}]
    
    current_id = int(current_user.get_id())
    dest = int(data.get('destination'))

    # updating the last message the user got
    new_msg_dict = {'messages': []}
    if not (current_id in messages_sent):  # if first time website reqest msgs
        messages_sent[current_id] = {}
        messages_sent[current_id][dest] = msg_dict['messages'][-1]['date']
        new_msg_dict = msg_dict
    elif not (dest in messages_sent[current_id]):  # if first time website requst chat with another user 
        messages_sent[current_id][dest] = msg_dict['messages'][-1]['date']
        new_msg_dict = msg_dict
    else:
        last_sent_msg = datetime.strptime(messages_sent[current_id][dest], "%Y-%m-%d %H:%M:%S.%f")
        for msg in msg_dict['messages'][::-1]: #run on messages
            cur_msg_date=datetime.strptime(msg['date'], "%Y-%m-%d %H:%M:%S.%f")
            if cur_msg_date == last_sent_msg: # as long as didnt send the msg allready 
                break
            new_msg_dict['messages'] += [msg]
        messages_sent[current_id][dest] = msg_dict['messages'][-1]['date']
    
    print(new_msg_dict)
    return new_msg_dict
