import nodes_records as nodes_records
import socket
import time
import os

UPDATE_TIME = 10

def updateNodeStatus(db_file):
    # Updating every number of seconds the state of all nodes in the db
    while True:
        rec = nodes_records.NodesRecords(db_file)
        addresses = get_nodes_addresse(rec)
        for address in addresses:
            status = True
            if not is_node_alive(address):
                status = False
                print('connection failed: ', address)

            rec.updateStatus(address[0], status=status)
        del rec
        time.sleep(UPDATE_TIME)

def is_node_alive(address):
    # Returning if the given node is up or not
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        res = ''
        try:
            s.connect(address)
            s.sendall(b'status')  # sending something to cause an error response
            res = s.recv(1024)
            print('recieved status: ', res)
        except Exception as e:
            print(e)
    return len(res) != 0
        

def get_nodes_addresse(rec):
    # Returning list of ip addresses and ports of each node in the records
    nodes = rec.get_nodes(is_alive=False)
    addresses = [(node[2], node[1]) for node in nodes]
    return addresses

def main():
    pass

if __name__ == '__main__':
    main()