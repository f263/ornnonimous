# OrNonimous - Tor protocol implementation 


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Launch
 * References
 
------------------------
INTRODUCTION
------------------------

>The Tor network is a secure, encrypted protocol that can ensure privacy for data and communications on the web. Short for the Onion Routing project, the system uses a series of layered nodes to hide IP addresses, online data, and browsing history.

OrNonimous implements the Tor protocol in python. The nodes are deployed on Docker containers and any client can create a circuit with those nodes using the onion proxy. This project also consists a Flask web application for chatting between users (under ./app).

All of the conversations are encrypted using symmetric AES encryption, and the symmetric keys are exchanged in the DF key exchange method using asymmetric RSA encryption.

-----------------
REQUIREMENTS
------------------

This project requires the following modules:

* Python 3.6 +
* [Flask](https://flask.palletsprojects.com/en/2.0.x/)
* [Docker](https://docs.docker.com/engine/install/ubuntu/)
* [Docker Compose](https://docs.docker.com/compose/)

--------------
INSTALLATION
------------
Install required python modules. Docker will install it automatically when being built.

```bash
$ pip install -r ./requirements.txt
```

Install Flask modules for the web app (optional):

```bash
$ pip install -U Flask
$ pip install -U Flask-SQLAlchemy
$ pip install flask-login
```

--------------
LAUNCH
-------------

Deploy the nodes and the directory node with the ```docker-compose.yml``` file:
```bash
$ sudo docker-compose up --build
```
There are 5 default nodes in the compose file, but you can add more.

### proxy configuration can be changed at ```config.py```
Run the proxy:
```bash
$ python3 ./onion_proxy/onion_proxy.py
```
You should be presented with a GUI to select the number of nodes to build a circuit with (3-5)

Run the Flask app on the local host port 5000 (optional):
```bash
$ export FLASK_APP=app
$ flask run --host=0.0.0.0
```

### Now the proxy can forward your packets encrypted and anonymous through the running nodes.


--------
REFERENCES
---------------
- [tor protocol specification](https://github.com/torproject/torspec/blob/main/tor-spec.txt)
- [How Does Tor Really Work?](https://skerritt.blog/how-does-tor-really-work/#how-is-a-circuit-created-)
- [Docker Tutorial](https://www.youtube.com/watch?v=fqMOX6JJhGo)
- [Flask Authentication](https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login)