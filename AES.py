from Cryptodome.Cipher import AES
from secrets import token_bytes  
from Cryptodome import Random

class Aes:
    def __init__(self, key = token_bytes(16)):
        self._key = key

    def encrypt(self, raw):
        raw = (self._pad(raw))  # payload should be the size of a block
        iv = Random.new().read( AES.block_size )
        cipher = AES.new(self._key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(raw)
    
    def decrypt(self, enc):
        iv = enc[:AES.block_size]
        cipher = AES.new(self._key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:]))

    def _pad(self, raw):
        # padding the payload with the value of the numebr of remaining bytes (should be devided by 16)
        return raw + (AES.block_size - (len(raw) % AES.block_size)) * (chr(AES.block_size - len(raw) % AES.block_size)).encode()

    def _unpad(self, raw):
        # reading the value of the padding representing the padding size and unpadding payload
        return raw[0:-ord(raw[-1:])]

    def get_key(self):
        return self._key
