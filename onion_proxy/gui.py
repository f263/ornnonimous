import PySimpleGUI as sg

def is_input_valid(a):
    # make sure the input is valid
    if a.isnumeric() == True:
        a = int(a)
        if a in [3,4,5]:
            return a
    return False

#this is the layout of the screen the user see(taxt box , color....)
layout = [
    [sg.Text("",key='error',text_color='#4BB6F3',background_color='#581845') ],
    [sg.Text("enter number of nodes: ",key='msg',background_color='#581845') ],
    [sg.In(size=(25,1),key='node_num')],
    [sg.Button('enter',enable_events =True),sg.Button('exit',enable_events =True)]
]


def get_node_num_gui():
    """
    the function show the screen and returns valid input of number of nodes 
    """
    node_num = 0
    window = sg.Window(title="onion proxy", layout = layout,margins=(150,100), background_color='#581845')
    while True:
        event, values = window.read()
        if event == 'enter':
            node_num = is_input_valid(values['node_num'])
            if node_num == False:
                window['error'].update("error enter only numbers 3-5")
            else:
                break
        elif event == 'exit' or event == sg.WIN_CLOSED:
            break
    
    window.close()
    return node_num


if __name__ == '__main__':
    print(get_node_num_gui())
